import React from 'react';
import ReactDOM from 'react-dom';
import Popup from '../components/Popup/Popup';

export const popup = {
  active: false,
  title: null,
  content: null,
};

export const showPopup = (title, content) => {
  popup.active = true;
  popup.title = title;
  popup.content = content;
  document.body.style.overflowY = 'hidden';
  ReactDOM.render(<Popup/>, document.getElementById('popup'));
};
