import React, { Component } from 'react';
import Header from './components/Header/Header';
import Stocks from './components/Stocks/Stocks';
import Services from './components/Services/Services';
import Certificates from './components/Certificates/Certificates';
import Gallery from './components/Gallery/Gallery';
import Contacts from './components/Contacts/Contacts';
import Footer from './components/Footer/Footer';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Stocks/>
        <Services/>
        <Certificates/>
        <Gallery/>
        <Contacts/>
        <Footer/>
        <div id="popup"/>
      </div>
    );
  }
}