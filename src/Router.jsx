import { Router } from 'react-router'
import createBrowserHistory from 'history/createBrowserHistory'

const history = createBrowserHistory()

  <Router history={history}>
  <App/>
  </Router>