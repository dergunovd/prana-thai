import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Slider from 'react-slick';
import './Gallery.sass';
import { Col, Grid, Row } from 'react-flexbox-grid';
import Tour from '../Tour/Tour';

export default class Gallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      src: `${this.baseDir}gallery_1.jpg`,
      alt: 'Фото №1',
    }
  }

  showTour = e => {
    e.preventDefault();
    ReactDOM.render(<Tour/>, document.getElementById('popup'));
  };

  setImg = e => {
    const elem = e.target;
    this.setState({src: elem.src, alt: elem.alt});
  };

  baseDir = '/examples/';
  imgs = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  slidesRender = () =>
    this.imgs.map(i => <img key={i} src={`${this.baseDir}gallery_${i}.jpg`} alt={`Фото №${i}`}
                            className="gallery__thumb" onClick={this.setImg}/>);

  render() {
    const settings = {
      dots: false,
      arrows: false,
      infinite: true,
      vertical: true,
      verticalSwiping: true,
      speed: 100,
      slidesToShow: 5,
      slidesToScroll: 2,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            vertical: false,
            verticalSwiping: false,
            slidesToShow: 3,
          }
        },
      ]
    };
    return (
      <section className="gallery" id="gallery">
        <h2>Галерея</h2>
        <Grid>
          <Row className="gallery__row">
            <Col md={10}>
              <img className="gallery__main__img" src={this.state.src} alt={this.state.alt}/>
            </Col>
            <Col className="gallery__thumbs" md={2}>
              <Slider {...settings}>
                {this.slidesRender()}
              </Slider>
            </Col>
          </Row>
        </Grid>
        <Grid>
          <Row center="xs">
            <a className="gallery__3d" onClick={this.showTour}>3D-тур</a>
          </Row>
        </Grid>
      </section>
    )
  }
}

