import React, { Component } from 'react';
import './Contacts.sass';
import { compose, withProps } from 'recompose';
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from 'react-google-maps';

const MapComponent = compose(
  withProps({
    googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBpsNkMqxjo_jXrXzqQvbH2bQwdtJtJ838&v=3.exp&libraries=geometry,drawing,places',
    loadingElement: <div style={{height: `100%`}}/>,
    containerElement: <div style={{height: `400px`}}/>,
    mapElement: <div style={{height: `100%`}}/>
  }),
  withScriptjs,
  withGoogleMap
)(() => (
  <GoogleMap defaultZoom={16} defaultCenter={{lat: 51.5360926, lng: 46.0148543}}>
    <Marker position={{lat: 51.5360926, lng: 46.0148543}}/>
  </GoogleMap>
));

export default class Contacts extends Component {
  render() {
    return (
      <section className="contacts" id="contacts">
        <MapComponent/>
        <h2 className="contacts__h2">Наш адрес: Большая&nbsp;Казачья,&nbsp;79/85</h2>
        <a className="contacts__btn">Записаться на сеанс</a>
      </section>
    );
  }
}