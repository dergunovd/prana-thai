import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Col, Grid, Row } from 'react-flexbox-grid';
import { popup } from '../../containers/popup';
import './Popup.sass';

export default class Popup extends Component {
  formatContent = () => {
    return {__html: popup.content};
  };

  close = e => {
    e.preventDefault();
    popup.active = false;
    popup.title = null;
    popup.content = null;
    ReactDOM.render(null, document.getElementById('popup'));
    document.body.style.overflowY = 'visible';
  };

  render() {
    return (
      <div className="popup">
        <Grid className="popup__inner">
          <Row>
            <Col xs={12}>
              <Col md={10} mdOffset={1}>
                <Row className="popup__header">
                  <Col xs={11}>
                    <div className="popup__header__title">{popup.title}</div>
                  </Col>
                  <Col xs={1}>
                    <a className="popup__header__close" onClick={this.close}>+</a>
                  </Col>
                </Row>
                <div className="popup__content" dangerouslySetInnerHTML={this.formatContent()}/>
              </Col>
            </Col>
          </Row>
        </Grid>
      </div>
    )
  }
}