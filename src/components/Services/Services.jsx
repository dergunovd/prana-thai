import React, { Component } from 'react';
import './Services.sass';
import { Col, Grid } from 'react-flexbox-grid';
import ServicesGroup from './ServicesGroup/ServicesGroup';
import servicesJson from './services.json';

export default class Services extends Component {
  servicesRender = () =>
    servicesJson.map((item, index) => <ServicesGroup key={index} name={item.name} items={item.items}/>);

  render() {
    return (
      <section className="services" id="services">
        <h2>Услуги</h2>
        <Grid>
          <Col md={11}>
            {this.servicesRender()}
          </Col>
        </Grid>
      </section>
    );
  }
}