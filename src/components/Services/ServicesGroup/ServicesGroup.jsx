import React, { Component } from 'react';
import './ServicesGroup.sass';
import ServicesItem from './ServicesItem/ServicesItem';
import { Col, Row } from 'react-flexbox-grid';
import Slider from 'react-slick';

export default class ServicesGroup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      active: false,
      hover: false
    }
  }

  servicesItemRender = () =>
    this.props.items.map((item, index) => <ServicesItem key={index} name={item.name} times={item.times}
                                                        description={item.description} price={item.price}/>);
  btnClick = () => {
    this.setState({active: !this.state.active});
  };

  mouseEnter = () => {
    this.setState({hover: true})
  };

  mouseLeave = () => {
    this.setState({hover: false})
  };

  render() {
    const settings = {
      dots: true,
      infinite: false,
      speed: 100,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    };
    const serviceGroupClass = 'services__group' + (this.state.active ? ' services__group_active' : '');
    const serviceGroupBtnClass = 'services__group__btn'
      + (this.state.active ? ' services__group__btn_active' : '')
      + (this.state.hover ? ' services__group__btn_hover' : '');
    return (
      <Row className={serviceGroupClass}>
        <Col xs={10} md={10} mdOffset={1} onClick={this.btnClick} onMouseEnter={this.mouseEnter}
             onMouseLeave={this.mouseLeave}
             className="services__group__name">{this.props.name}</Col>
        <Col xs={2} md={1} onClick={this.btnClick} onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave}
             className={serviceGroupBtnClass}>+</Col>
        <Col xs={12} className={'services__group__items' + (this.state.active ? ' services__group__items_active' : '')}>
          <Slider {...settings}>
            {this.servicesItemRender()}
          </Slider>
        </Col>
      </Row>
    )
  };
}
