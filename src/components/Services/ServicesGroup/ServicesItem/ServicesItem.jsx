import React, { Component } from 'react';
import { showPopup } from '../../../../containers/popup';
import './ServicesItem.sass';

export default class ServicesItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: '0'
    }
  }

  showPopup = (e) => {
    e.preventDefault();
    showPopup(this.props.name, this.props.description);
  };

  clickTime = (e) => {
    this.setState({time: e.target.dataset.index});
  };

  servicesItemTimesRender = () =>
    this.props.times.map((item, index) => <a key={index} data-index={index} onClick={this.clickTime}
                                             className={'services__item__time' + (index == this.state.time ? ' services__item__time_active' : '')}>{item}</a>);

  render() {
    return (
      <div className="services__item">
        <div className="services__item__name">{this.props.name}</div>
        <div className="services__item__times">
          {this.servicesItemTimesRender()}
        </div>
        {this.props.description
          ? <a className="services__item__more" onClick={this.showPopup}>Подробнее</a>
          : <a className="services__item__more">&nbsp;</a>}
        <div className="services__item__price">{this.props.price[this.state.time]} ₽</div>
      </div>
    )
  }
}
