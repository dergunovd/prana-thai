import * as THREE from 'three';
// import 'DeviceOrientationControls';

export default class Panorama {
  target = new THREE.Vector3();
  lon = 90;
  lat = 0;
  phi = 0;
  theta = 0;

  init() {
    THREE.DeviceOrientationControls = function (object) {
      const scope = this;
      this.object = object;
      this.object.rotation.reorder('YXZ');
      this.enabled = true;
      this.deviceOrientation = {};
      this.screenOrientation = 0;
      this.alphaOffset = 0; // radians

      const onDeviceOrientationChangeEvent = function (event) {
        scope.deviceOrientation = event;
      };

      const onScreenOrientationChangeEvent = function () {
        scope.screenOrientation = window.orientation || 0;
      };

      // The angles alpha, beta and gamma form a set of intrinsic Tait-Bryan angles of type Z-X'-Y''

      const setObjectQuaternion = function () {
        const zee = new THREE.Vector3(0, 0, 1);
        const euler = new THREE.Euler();
        const q0 = new THREE.Quaternion();
        const q1 = new THREE.Quaternion(-Math.sqrt(0.5), 0, 0, Math.sqrt(0.5)); // - PI/2 around the x-axis
        return function (quaternion, alpha, beta, gamma, orient) {
          euler.set(beta, alpha, -gamma, 'YXZ'); // 'ZXY' for the device, but 'YXZ' for us
          quaternion.setFromEuler(euler); // orient the device
          quaternion.multiply(q1); // camera looks out the back of the device, not the top
          quaternion.multiply(q0.setFromAxisAngle(zee, -orient)); // adjust for screen orientation
        };
      }();

      this.connect = function () {
        onScreenOrientationChangeEvent(); // run once on load
        window.addEventListener('orientationchange', onScreenOrientationChangeEvent, false);
        window.addEventListener('deviceorientation', onDeviceOrientationChangeEvent, false);
        scope.enabled = true;
      };

      this.disconnect = function () {
        window.removeEventListener('orientationchange', onScreenOrientationChangeEvent, false);
        window.removeEventListener('deviceorientation', onDeviceOrientationChangeEvent, false);
        scope.enabled = false;
      };

      this.update = function () {
        if (scope.enabled === false) return;
        const device = scope.deviceOrientation;
        if (device) {
          const alpha = device.alpha ? THREE.Math.degToRad(device.alpha) + scope.alphaOffset : 0; // Z
          const beta = device.beta ? THREE.Math.degToRad(device.beta) : 0; // X'
          const gamma = device.gamma ? THREE.Math.degToRad(device.gamma) : 0; // Y''
          const orient = scope.screenOrientation ? THREE.Math.degToRad(scope.screenOrientation) : 0; // O
          setObjectQuaternion(scope.object.quaternion, alpha, beta, gamma, orient);
          return device.alpha || device.beta || device.gamma;
        }
      };

      this.dispose = function () {
        scope.disconnect();
      };

      this.connect();
    };
    THREE.CSS3DObject = function (element) {
      THREE.Object3D.call(this);
      this.element = element;
      this.element.style.position = 'absolute';
      this.addEventListener('removed', function () {
        if (this.element.parentNode !== null) {
          this.element.parentNode.removeChild(this.element);
        }
      });
    };
    THREE.CSS3DObject.prototype = Object.create(THREE.Object3D.prototype);
    THREE.CSS3DObject.prototype.constructor = THREE.CSS3DObject;
    THREE.CSS3DSprite = function (element) {
      THREE.CSS3DObject.call(this, element);
    };

    THREE.CSS3DSprite.prototype = Object.create(THREE.CSS3DObject.prototype);
    THREE.CSS3DSprite.prototype.constructor = THREE.CSS3DSprite;

    THREE.CSS3DRenderer = function () {
      const getDistanceToSquared = function () {

        const a = new THREE.Vector3();
        const b = new THREE.Vector3();

        return function (object1, object2) {
          a.setFromMatrixPosition(object1.matrixWorld);
          b.setFromMatrixPosition(object2.matrixWorld);
          return a.distanceToSquared(b);
        };

      }();
      console.log('THREE.CSS3DRenderer', THREE.REVISION);
      let _width, _height;
      let _widthHalf, _heightHalf;
      const matrix = new THREE.Matrix4();
      const cache = {
        camera: {fov: 0, style: ''},
        objects: new WeakMap()
      };
      const domElement = document.createElement('div');
      domElement.style.overflow = 'hidden';
      this.domElement = domElement;
      const cameraElement = document.createElement('div');

      cameraElement.style.WebkitTransformStyle = 'preserve-3d';
      cameraElement.style.transformStyle = 'preserve-3d';

      domElement.appendChild(cameraElement);

      let isIE = /Trident/i.test(navigator.userAgent);

      this.getSize = () => {
        return {
          width: _width,
          height: _height
        };
      };

      this.setSize = (width, height) => {
        _width = width;
        _height = height;
        _widthHalf = _width / 2;
        _heightHalf = _height / 2;
        domElement.style.width = width + 'px';
        domElement.style.height = height + 'px';
        cameraElement.style.width = width + 'px';
        cameraElement.style.height = height + 'px';
      };

      const epsilon = value => Math.abs(value) < 1e-10 ? 0 : value;

      const getCameraCSSMatrix = matrix => {
        const elements = matrix.elements;
        return 'matrix3d(' +
          epsilon(elements[0]) + ',' +
          epsilon(-elements[1]) + ',' +
          epsilon(elements[2]) + ',' +
          epsilon(elements[3]) + ',' +
          epsilon(elements[4]) + ',' +
          epsilon(-elements[5]) + ',' +
          epsilon(elements[6]) + ',' +
          epsilon(elements[7]) + ',' +
          epsilon(elements[8]) + ',' +
          epsilon(-elements[9]) + ',' +
          epsilon(elements[10]) + ',' +
          epsilon(elements[11]) + ',' +
          epsilon(elements[12]) + ',' +
          epsilon(-elements[13]) + ',' +
          epsilon(elements[14]) + ',' +
          epsilon(elements[15]) + ')';
      };

      const getObjectCSSMatrix = (matrix, cameraCSSMatrix) => {
        const elements = matrix.elements;
        const matrix3d = 'matrix3d(' +
          epsilon(elements[0]) + ',' +
          epsilon(elements[1]) + ',' +
          epsilon(elements[2]) + ',' +
          epsilon(elements[3]) + ',' +
          epsilon(-elements[4]) + ',' +
          epsilon(-elements[5]) + ',' +
          epsilon(-elements[6]) + ',' +
          epsilon(-elements[7]) + ',' +
          epsilon(elements[8]) + ',' +
          epsilon(elements[9]) + ',' +
          epsilon(elements[10]) + ',' +
          epsilon(elements[11]) + ',' +
          epsilon(elements[12]) + ',' +
          epsilon(elements[13]) + ',' +
          epsilon(elements[14]) + ',' +
          epsilon(elements[15]) + ')';
        if (isIE) {
          return 'translate(-50%,-50%)' +
            'translate(' + _widthHalf + 'px,' + _heightHalf + 'px)' +
            cameraCSSMatrix +
            matrix3d;
        }
        return 'translate(-50%,-50%)' + matrix3d;
      };

      const renderObject = (object, camera, cameraCSSMatrix) => {
        if (object instanceof THREE.CSS3DObject) {
          let style;
          if (object instanceof THREE.CSS3DSprite) {
            matrix.copy(camera.matrixWorldInverse);
            matrix.transpose();
            matrix.copyPosition(object.matrixWorld);
            matrix.scale(object.scale);
            matrix.elements[3] = 0;
            matrix.elements[7] = 0;
            matrix.elements[11] = 0;
            matrix.elements[15] = 1;
            style = getObjectCSSMatrix(matrix, cameraCSSMatrix);
          } else {
            style = getObjectCSSMatrix(object.matrixWorld, cameraCSSMatrix);
          }
          const element = object.element;
          const cachedStyle = cache.objects.get(object);
          if (cachedStyle === undefined || cachedStyle !== style) {
            element.style.WebkitTransform = style;
            element.style.transform = style;
            const objectData = {style: style};
            if (isIE) {
              objectData.distanceToCameraSquared = getDistanceToSquared(camera, object);
            }
            cache.objects.set(object, objectData);
          }
          if (element.parentNode !== cameraElement) {
            cameraElement.appendChild(element);
          }
        }

        let i = 0, l = object.children.length;
        for (; i < l; i++) {
          renderObject(object.children[i], camera, cameraCSSMatrix);
        }
      };


      function filterAndFlatten(scene) {
        const result = [];
        scene.traverse(function (object) {
          if (object instanceof THREE.CSS3DObject) result.push(object);
        });
        return result;
      }

      function zOrder(scene) {
        const sorted = filterAndFlatten(scene).sort((a, b) => {
          const distanceA = cache.objects.get(a).distanceToCameraSquared;
          const distanceB = cache.objects.get(b).distanceToCameraSquared;
          return distanceA - distanceB;
        });
        const zMax = sorted.length;
        let i = 0, l = sorted.length;
        for (; i < l; i++) {
          sorted[i].element.style.zIndex = zMax - i;
        }
      }

      this.render = (scene, camera) => {
        const fov = camera.projectionMatrix.elements[5] * _heightHalf;

        if (cache.camera.fov !== fov) {
          if (camera.isPerspectiveCamera) {
            domElement.style.WebkitPerspective = fov + 'px';
            domElement.style.perspective = fov + 'px';
          }
          cache.camera.fov = fov;
        }

        scene.updateMatrixWorld();
        if (camera.parent === null) {
          camera.updateMatrixWorld();
        }
        const cameraCSSMatrix = camera.isOrthographicCamera ?
          'scale(' + fov + ')' + getCameraCSSMatrix(camera.matrixWorldInverse) :
          'translateZ(' + fov + 'px)' + getCameraCSSMatrix(camera.matrixWorldInverse);

        const style = cameraCSSMatrix + 'translate(' + _widthHalf + 'px,' + _heightHalf + 'px)';

        if (cache.camera.style !== style && !isIE) {
          cameraElement.style.WebkitTransform = style;
          cameraElement.style.transform = style;
          cache.camera.style = style;
        }

        renderObject(scene, camera, cameraCSSMatrix);
        if (isIE) {
          // IE10 and 11 does not support 'preserve-3d'.
          // Thus, z-order in 3D will not work.
          // We have to calc z-order manually and set CSS z-index for IE.
          // FYI: z-index can't handle object intersection
          zOrder(scene);
        }
      };
    };

    this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000);
    this.scene = new THREE.Scene();
    this.controls = new THREE.DeviceOrientationControls(this.camera);
    const sides = [
      {
        url: 'room1/px.jpg',
        position: [-512, 0, 0],
        rotation: [0, Math.PI / 2, 0]
      },
      {
        url: 'room1/nx.jpg',
        position: [512, 0, 0],
        rotation: [0, -Math.PI / 2, 0]
      },
      {
        url: 'room1/py.jpg',
        position: [0, 512, 0],
        rotation: [Math.PI / 2, 0, Math.PI]
      },
      {
        url: 'room1/ny.jpg',
        position: [0, -512, 0],
        rotation: [-Math.PI / 2, 0, Math.PI]
      },
      {
        url: 'room1/pz.jpg',
        position: [0, 0, 512],
        rotation: [0, Math.PI, 0]
      },
      {
        url: 'room1/nz.jpg',
        position: [0, 0, -512],
        rotation: [0, 0, 0]
      }
    ];
    const cube = new THREE.Object3D();
    this.scene.add(cube);
    for (let i = 0; i < sides.length; i++) {
      const side = sides[i];
      const element = document.createElement('img');
      element.width = 1028; // 2 pixels extra to close the gap.
      element.src = side.url;
      const object = new THREE.CSS3DObject(element);
      object.position.fromArray(side.position);
      object.rotation.fromArray(side.rotation);
      cube.add(object);
    }
    this.renderer = new THREE.CSS3DRenderer();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    document.getElementById('tour').appendChild(this.renderer.domElement);
    //
    document.addEventListener('mousedown', this.onDocumentMouseDown, false);
    document.addEventListener('wheel', this.onDocumentMouseWheel, false);
    document.addEventListener('touchstart', this.onDocumentTouchStart, false);
    document.addEventListener('touchmove', this.onDocumentTouchMove, false);
    window.addEventListener('resize', this.onWindowResize, false);
  }

  onDocumentMouseDown(event) {
    event.preventDefault();
    document.addEventListener('mousemove', this.onDocumentMouseMove, false);
    document.addEventListener('mouseup', this.onDocumentMouseUp, false);
  }

  onDocumentMouseMove(event) {
    const movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
    const movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;
    this.lon -= movementX * 0.1;
    this.lat += movementY * 0.1;
  }

  onDocumentMouseUp(event) {
    document.removeEventListener('mousemove', this.onDocumentMouseMove);
    document.removeEventListener('mouseup', this.onDocumentMouseUp);
  }

  onDocumentMouseWheel(event) {
    const fov = this.camera.fov + event.deltaY * 0.05;
    this.camera.fov = THREE.Math.clamp(fov, 10, 75);
    this.camera.updateProjectionMatrix();
  }

  onDocumentTouchStart(event) {
    event.preventDefault();
    const touch = event.touches[0];
    this.touchX = touch.screenX;
    this.touchY = touch.screenY;
  }

  onDocumentTouchMove(event) {
    event.preventDefault();
    const touch = event.touches[0];
    this.lon -= (touch.screenX - this.touchX) * 0.1;
    this.lat += (touch.screenY - this.touchY) * 0.1;
    this.touchX = touch.screenX;
    this.touchY = touch.screenY;
  }

  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  animate() {
    requestAnimationFrame(this.animate);
    if (!this.controls.update()) {
      this.lat = Math.max(-85, Math.min(85, this.lat));
      this.phi = THREE.Math.degToRad(90 - this.lat);
      this.theta = THREE.Math.degToRad(this.lon);
      this.target.x = Math.sin(this.phi) * Math.cos(this.theta);
      this.target.y = Math.cos(this.phi);
      this.target.z = Math.sin(this.phi) * Math.sin(this.theta);
      this.camera.lookAt(this.target);
    }
    this.renderer.render(this.scene, this.camera);
  }
}