import React, { Component } from 'react';
import './Tour.sass'
import ReactDOM from 'react-dom';

export default class Tour extends Component {
  constructor(props) {
    super(props);
    this.state = {active: true};
  }

  closeTour = e => {
    e.preventDefault();
    document.body.style.overflowY = 'visible';
    this.setState({active: false});
    ReactDOM.render(null, document.getElementById('popup'));
  };

  render() {
    document.body.style.overflowY = 'hidden';
    const closeClass = 'tour__close' + (this.state.active ? ' tour__close_active' : '');
    return (this.state.active
      ? <div className="tour">
        <div id="tour">
          <iframe className="tour__frame" title="3D Tour" src="/panorama/panorama.html"/>
        </div>
        <a className={closeClass} onClick={this.closeTour}>Закрыть</a></div>
      : null);
  }
}