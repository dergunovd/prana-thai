import React, { Component } from 'react';
import Menu from './Menu/Menu';
import './Header.sass';

export default class Stocks extends Component {
  render() {
    return (
      <header className="header">
        <div className="header__logo"/>
        <Menu/>
      </header>
    )
  }
}
