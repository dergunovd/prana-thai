import React, { Component } from 'react';
import './Certificates.sass';
import { Col, Grid, Row } from 'react-flexbox-grid';
import certImg from '../../img/certificates.png';

export default class Certificates extends Component {
  render() {
    return (
      <section className="certificates" id="certificates">
        <h2>Сертификаты</h2>
        <Grid>
          <Row>
            <Col md={6}>
              <p className="certificates__p">Вы можете приобрести сертификат нашего салона на&nbsp;сумму от&nbsp;1000
                и&nbsp;до&nbsp;10&nbsp;000
                рублей или&nbsp;на&nbsp;конкреткую&nbsp;<a className="context-link" href="#services">услугу.</a></p>
              <p className="certificates__p">Просто свяжитесь с&nbsp;нами и&nbsp;назовите номинал сертификата
                или&nbsp;услугу.</p>
              <p className="certificates__p">Внимание о&nbsp;здоровье, забота о&nbsp;красоте, маленькое путешествие
                в&nbsp;солнечный Таиланд – такой
                подарок безусловно оценят, не&nbsp;забудут и&nbsp;обязательно поблагодарят!</p>
            </Col>
            <Col sm={12} md={5} mdOffset={1}>
              <img className="certificates__img" src={certImg} alt={'Сертификаты'}/>
            </Col>
          </Row>
        </Grid>
      </section>
    )
  }
}