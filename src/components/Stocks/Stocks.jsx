import React, { Component } from 'react';
import Slider from 'react-slick';
import Stock from './Stock/Stock';
import './Stocks.sass';

export default class Stocks extends Component {

  render() {
    const settings = {
      dots: true,
      infinite: true,
      centerMode: true,
      speed: 100,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return (
      <section className="stocks" id="stocks">
        <h2>Акции</h2>
        <Slider {...settings}>
          <Stock name="Знакомство с таинством Тайланда" photo="/examples/stocks_1.jpg"
                 description="На первое посещение скидка 15%"/>
          <Stock name="Скидка на массаж в будни<br>с 10:00 до 14:00" photo="/examples/stocks_2.jpg"
                 description="На все виды массажа в будние дни с 10:00 до 14:00 — скидка 20%<br>На комплексные и СПА-программы скидка не распространяется"/>
          <Stock name="Скидка именинникам" photo="/examples/stocks_3.jpg"
                 description="Всем именинникам скидка 20% на все виды массажа.<br>Две недели до дня рождения и две недели после дня рождения.<br>На комплексные и СПА-программы скидка не распространяется."/>
          <Stock name="4-ая услуга в подарок" photo="/examples/stocks_2.jpg"
                 description="Купите три одинаковых массажа, получите четвертый в подарок"/>
        </Slider>
      </section>
    )
  }
}