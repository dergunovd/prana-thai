import React, { Component } from 'react';
import './Stock.sass';

export default class Stock extends Component {

  formatTitle = () => {
    return {__html: this.props.name};
  };

  formatDescription = () => {
    return {__html: this.props.description};
  };

  render() {
    return (
      <div className="stocks__slider__item">
        <img className="stocks__slider__img" alt={this.props.name} src={this.props.photo}/>
        <div className="stocks__slider__content">
          <h3 className="stocks__slider__title" dangerouslySetInnerHTML={this.formatTitle()}/>
          <p className="stocks__slider__description" dangerouslySetInnerHTML={this.formatDescription()}/>
        </div>
      </div>
    )
  }
}